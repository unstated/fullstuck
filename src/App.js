import React, { useContext } from 'react';
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom';

import './App.css';
import AppContext from './AppContext';
import Layout from './layout/Layout';
import Home from './pages/Home/Home';
import MyProfile from './pages/MyProfile/MyProfile';

function App() {
  return (
    <BrowserRouter>
      <AppContext.Provider value='hi'>
        <Layout>
          <Switch>
            <Route path='/' exact default component={Home} />
            <Route path='/fsfmksdfjenwek' component={MyProfile} />
          </Switch>
        </Layout>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
