import React, { useContext } from 'react';
import AppContext from '../../AppContext';

const Home = () => {
  const msg = useContext(AppContext);

  return <div>{msg}</div>;
};

export default Home;
